The expected behavior is for IGB to immediately trigger a warning and filter out and file tags with this content.  The warning should list all file tags with similar illegal content.
